﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class childPage_UserControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

  
      // string Page_url = "~"+Request.Url.AbsolutePath;
       // BindRepeater();

    }


    public void BindRepeater()
    {
        string Page_url = "~"+ Request.Url.AbsolutePath;
        try
        {

           // int Role_id =3;

           int Role_id = Convert.ToInt32(Session["Role_Id"]) ;

            string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);



            SqlCommand cmd = new SqlCommand("ChildPageSp_PP");
            cmd.Connection = con;
              con.Open();

                   cmd.CommandType = CommandType.StoredProcedure;

                   cmd.Parameters.AddWithValue("@Role_id", Role_id);
                   cmd.Parameters.AddWithValue("@pageurl", Page_url);

                   SqlDataAdapter sda = new SqlDataAdapter();
                   sda.SelectCommand = cmd;
                   DataTable dt = new DataTable();

                   sda.Fill(dt);


                   Repeater1.DataSource = dt;


              
                   Repeater1.DataBind();
                   con.Close();
                }
            

        
        catch (Exception ex)
        {
            throw ex;
        }
      
        
       
    }


}