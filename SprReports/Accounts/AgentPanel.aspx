﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="AgentPanel.aspx.vb" Inherits="Reports_Accounts_AgentPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <%-- 
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />--%>

    <script type="text/javascript" language="javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }

        function isChar(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 32) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }
    
    </script>

    <%--<script src="JS/JScript.js" type="text/javascript"></script>--%>

    <script type="text/javascript" language="javascript">
        function ValidateAgent() {

            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_amount").value == "") {
                alert('Please Fill Amount');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_amount").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Select Payment Mode") {
                alert('Please Select Payment Mode');
                document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").focus();
                return false;
            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cash") {

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_city").value == "") {
                    alert('Please Fill Deposite City');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_city").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_depositedate").value == "") {
                    alert('Please Fill Deposite Date');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_depositedate").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_Office").value == "--Select--") {
                    alert('Please Select Office');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_Office").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_concernperson").value == "") {
                    alert('Please Fill Concern Person');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_concernperson").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }


            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cash Deposite In Bank") {
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_BranchCode").value == "") {
                    alert('Please Fill Branch Code');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_BranchCode").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cheque") {

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_chequedate").value == "") {
                    alert('Please Fill Cheque Date');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_chequedate").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_chequeno").value == "") {
                    alert('Please Fill Cheque Number');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_chequeno").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_BankName").value == "") {
                    alert('Please Fill Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_BankName").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "NetBanking" || document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "RTGS") {
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_tranid").value == "") {
                    alert('Please Fill Transaction ID');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_tranid").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }

        }


    </script>

    <table class="w70 auto boxshadow">
        <tr>
            
                        <td align="left" style="color:#fff; font-weight:bold; padding-top:7px;" colspan="4">
                            Deposit Request Form
                        </td>
                    </tr>
                    <tr><td class="clear1" colspan="4"></td></tr>
                        <tr>
                            <td class="fltdtls" colspan="4" id="td_UploadType" runat="server" visible="false">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" height="30" style="font-weight: bold;" width="200">
                                            Uploade Request Type:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="RBL_UploadType" runat="server" RepeatDirection="Horizontal"
                                                Width="300px">
                                                <asp:ListItem Value="CA" Selected="True"> Fresh Upload</asp:ListItem>
                                                <asp:ListItem Value="AD"> Adjustment</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="30px" class="fltdtls">
                                Amount:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txt_amount" runat="server" MaxLength="10" onKeyPress="return isNumberKey(event)"
                                    Width="110px"></asp:TextBox>
                            </td>
                            <td class="fltdtls" width="120">
                                Mode Of Payment:
                            </td>
                            <td width="160px">
                                <asp:DropDownList ID="ddl_modepayment" runat="server" AutoPostBack="True" Width="150px">
                                </asp:DropDownList>
                            </td>
                            
                        </tr>
        <tr>
            <td colspan="4">
            <table class="w100">
                        <tr id="check_info" runat="server" visible="false">
                            <td class="fltdtls" height="30px" width="110px">
                                Cheque Date:
                            </td>
                            <td width="140px">
                                <asp:TextBox ID="txt_chequedate" runat="server" Width="110px"></asp:TextBox>
                                <a href="javascript:cal3.popup();">
                                    <img src="../../images/cal.gif" alt="Click Here to Pick up the date" width="16" height="16"
                                        border="0" align="middle" /></a>
                            </td>
                            <td width="120px" class="fltdtls">
                                Cheque No:
                            </td>
                            <td width="140">
                                <asp:TextBox ID="txt_chequeno" runat="server" Width="110px"></asp:TextBox>
                            </td>
                            <td class="fltdtls">
                                Bank Name:
                            </td>
                            <td>
                                <asp:TextBox ID="txt_BankName" runat="server" Width="110px" AutoPostBack="True"></asp:TextBox>
                            </td>
                        </tr>
                </table></td></tr>
                        <tr>
                            <td width="110px" align="left" id="td_Bank" visible="false" runat="server" class="fltdtls"
                                height="30px">
                                Deposite Bank:
                            </td>
                            <td width="140px" id="td_Bank1" visible="false" runat="server">
                                <asp:DropDownList ID="ddl_BankName" runat="server" AutoPostBack="True" Width="117px">
                                </asp:DropDownList>
                            </td>
                            <td colspan="2" id="td_BranchAcc" runat="server" visible="false">
                                <table width="100%">
                                    <tr>
                                <td class="fltdtls">
                                    Deposite Branch :
                                </td>
                                <td width="140">
                                    <asp:DropDownList ID="ddl_Banch" runat="server" Width="130px">
                                    </asp:DropDownList>
                                </td>
                                <td class="fltdtls">
                                    Deposite A/C No:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddl_Account" runat="server" Width="117px">
                                    </asp:DropDownList>
                                </td>
                                        </tr></table>
                            </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table class="w100" id="div_Bankinfo" runat="server" visible="false">
                                        <tr>
                               
                                    <td id="td_BACode" runat="server" visible="false" class="fltdtls">
                                        Bank Area Code:
                                    </td>
                                    <td id="td_BACode1" runat="server" visible="false">
                                        <asp:TextBox ID="txt_areacode" runat="server" Width="110px"></asp:TextBox>
                                    </td>
                                    <td class="fltdtls" height="30" id="td_transid" visible="false" runat="server">
                                        Transaction ID:
                                    </td>
                                    <td width="120" id="td_transid1" visible="false" runat="server">
                                        <asp:TextBox ID="txt_tranid" runat="server" Width="110px"></asp:TextBox>
                                    </td>
                                    <td class="fltdtls" width="100px" height="30px" id="td_BCode" visible="false" runat="server">
                                        Branch Code:
                                    </td>
                                    <td id="td_BCode1" visible="false" runat="server">
                                        <asp:TextBox ID="txt_BranchCode" runat="server" Width="110px"></asp:TextBox>
                                    </td>
                               
                            </tr></table></td>
                        </tr>
        <tr><td colspan="4"><table id="tr_Deposite" runat="server" visible="false">
                        <tr >
                            <td class="fltdtls" height="30px" width="110px">
                                Deposit City:
                            </td>
                            <td width="140">
                                <asp:TextBox ID="txt_city" runat="server" Width="110px" onKeyPress="return isChar(event)"></asp:TextBox>
                            </td>
                            <td class="fltdtls" width="120px">
                                Deposite Date:
                            </td>
                            <td width="140">
                                <asp:TextBox ID="txt_depositedate" runat="server" Width="110px"></asp:TextBox>
                                <a href="javascript:cal4.popup();">
                                    <img src="../../images/cal.gif" alt="Click Here to Pick up the date" width="16" height="16"
                                        border="0" align="middle" /></a>
                            </td>
                            <%--  <div id="td_ConcernPerson" runat="server">--%>
                            <td height="30px" class="fltdtls">
                                Deposite Office:
                            </td>
                            <td>
                                <%-- <asp:TextBox ID="TextBox1" runat="server" Width="110px"></asp:TextBox>--%>
                                <asp:DropDownList ID="ddl_Office" runat="server" Width="110px">
                                    <%--<asp:ListItem Value="--Select--">--Select --</asp:ListItem>
                                    <asp:ListItem Value="Head Office">Head Office</asp:ListItem>
                                    <asp:ListItem Value=""></asp:ListItem>
                                    <asp:ListItem Value="Karol Bagh">Karol Bagh</asp:ListItem>
                                    <asp:ListItem Value="Rajouri Garden">Rajouri Garden</asp:ListItem>
                                    <asp:ListItem Value="Lucknow">Lucknow</asp:ListItem>
                                    <asp:ListItem Value="Kolkata">Kolkata</asp:ListItem>
                                    <asp:ListItem Value="Punjab">Punjab</asp:ListItem>
                                    <asp:ListItem Value="Jammu">Jammu</asp:ListItem>
                                    <asp:ListItem Value="Indore">Indore</asp:ListItem>
                                    <asp:ListItem Value="Faridabad">Faridabad</asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                            <%-- </div>--%>
                        </tr></table></td></tr>
         <tr ><td colspan="4"><table id="tr_conper" runat="server" visible="false">
                       
                        <tr >
                            <td height="30" class="fltdtls">
                                Concern Person:
                            </td>
                            <td height="30">
                                <asp:TextBox ID="txt_concernperson" runat="server" Width="110px" onKeyPress="return isChar(event)"></asp:TextBox>
                            </td>
                            <td class="fltdtls" height="30">
                                Receipt No:
                            </td>
                            <td>
                                <asp:TextBox ID="txt_ReceiptNo" runat="server" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr></table></td></tr>
                        <tr>
                            <td class="fltdtls" width="110px">
                                Remark:
                            </td>
                            <td style="padding-top: 6px; padding-bottom: 10px;" colspan="3">
                                <asp:TextBox ID="txt_remark" runat="server" TextMode="MultiLine" Height="80px" Width="400px"></asp:TextBox>
                            </td>
                           
                        </tr>
        <tr> <td valign="bottom" style="padding-bottom: 10px" colspan="4">
                                &nbsp;&nbsp;
                                <asp:Button ID="btn_submitt" Text="Submit" runat="server" CssClass="button rgt" OnClientClick="return ValidateAgent();" />
                                &nbsp;
                                <asp:Label ID="lblmsg" runat="server"></asp:Label>
                            </td></tr>
                    </table>
                

    <script type="text/javascript">

        if (document.getElementById('ctl00_ContentPlaceHolder1_ddl_modepayment').value == "Cheque") {
            var cal3 = new calendar1(document.forms['aspnetForm'].elements['ctl00_ContentPlaceHolder1_txt_chequedate']);
            cal3.year_scroll = true;
            cal3.time_comp = true;
        }
        if (document.getElementById('ctl00_ContentPlaceHolder1_ddl_modepayment').value == "Cash") {
            var cal4 = new calendar1(document.forms['aspnetForm'].elements['ctl00_ContentPlaceHolder1_txt_depositedate']);
            cal4.year_scroll = true;
            cal4.time_comp = true;
        }
    </script>

</asp:Content>
