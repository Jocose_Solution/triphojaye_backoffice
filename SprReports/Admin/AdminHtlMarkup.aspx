﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterAfterLogin.master" CodeFile="AdminHtlMarkup.aspx.vb" Inherits="SprReports_Admin_AdminHtlMarkup" %>

<%@ Register Src="~/UserControl/HotelSettings.ascx" TagPrefix="uc1" TagName="HotelSettings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Hotel/JS/HotelMarkup.js") %>" type="text/javascript"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <link href="<%=ResolveUrl("../../Hotel/css/B2Bhotelengine.css")%>" rel="stylesheet" type="text/css" />

    <div class="row">
        <div class="col-md-12" style="padding: 30px 30px 30px 80px;">
            <div class="col-md-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">HOTEL MARKUP</h3>
                        <input type="hidden" id="From" name="From" value="" />
                        <input type="hidden" id="To" name="TO" value="" />
                        <input type="hidden" id="hidtxtDepDate" name="hidtxtDepDate" value="" />
                        <input type="hidden" id="hidtxtRetDate" name="hidtxtRetDate" value="" />
                    </div>
                    <div class="panel-body">
                       
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Star:</label>
                                    <asp:DropDownList runat="server" ID="ddlStar" CssClass="form-control">
                                        <asp:ListItem Value="">ALL</asp:ListItem>
                                        <asp:ListItem Value="0">0</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Country:</label>
                                    <input type="text" name="TR_Country" id="TR_Country" value="ALL" class="form-control" />
                                    <input type="hidden" id="TR_Country1" name="TR_Country1" value="" />

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">City Name: </label>
                                    <input type="text" name="htlCity" id="htlCity" value="ALL" class="form-control" />
                                    <input type="hidden" id="htlcitylist" name="htlcitylist" value="" />
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"><span id="lblMrkAmt">Markup Amount</span></label>
                                    <asp:TextBox ID="txtAmt" runat="server" onKeyPress="return checkit(event)" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Markup Type:</label>
                                    <input type="radio" name="mrkupType" value="Fixed" id="rdbFixed" checked="checked" />
                                    <asp:Label ID="lblFixed" runat="server" Text="Fixed"></asp:Label>
                                    <input type="radio" name="mrkupType" value="Percentage" id="rdbPercentage" />
                                    <asp:Label ID="lblPercentage" runat="server" Text="Percentage"></asp:Label>
                                    <input id="mrktype" type="hidden" name="mrktype" value="Fixed" />

                                </div>
                            </div>
                        </div>
                           <div class="row">
                             <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Supplier</label>
                              <asp:DropDownList runat="server" ID="ddlSupplier" CssClass="form-control">
                                 <asp:ListItem Value="ALL" Text="ALL" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="ZUMATA" Text="ZUMATA"></asp:ListItem>
                                        <asp:ListItem Value="GAL" Text="GAL"></asp:ListItem> 
                                   <asp:ListItem Value="OffLineHotel" Text="OffLineHotel"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                            </div>
                       <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">GroupType</label>
                                    <asp:DropDownList ID="ddl_GroupType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agency Name:</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" value="ALL" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                    <input type="hidden" id="Hidden1" name="Agency" value="AdminAgency" />
                                </div>
                            </div>
                                     <div class="col-md-2">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_Submit" runat="server" Text="New Entry" OnClientClick="return MarkupValidation()" CssClass="button buttonBlue" />&nbsp;&nbsp;

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_Search" runat="server" Text="Search" CssClass="button buttonBlue" />
                                    <input type="hidden" id="Agency" name="Agency" value="Agent" />
                                </div>
                            </div>
                               </div>
                        <div class="large-12 medium-12 small-12" id="NoRecard" runat="server"></div>
                        <div class="clear1"></div>
             
                <div class="clearfix"></div>
                <div class="row">

                    <div class="col-md-12" style="background-color: white;">

                        <asp:UpdatePanel ID="BlockAirlineUpdPanel" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GrdMarkup" runat="server" AllowSorting="True" AutoGenerateColumns="False" CssClass="table"
                                    GridLines="None" AllowPaging="true" PageSize="100" OnPageIndexChanging="GrdMarkup_PageIndexChanging">
                                    <Columns>
                                        <asp:BoundField DataField="Supplier" HeaderText="Supplier" ReadOnly="true" />
                                        <asp:BoundField DataField="GroupType" HeaderText="GroupType" ReadOnly="true" />
                                        <asp:BoundField DataField="AgentID" HeaderText="AGENT ID" ReadOnly="true" />                                         
                                        <asp:BoundField DataField="Country" HeaderText="COUNTRY" ReadOnly="true" />
                                        <asp:BoundField DataField="City" HeaderText="CITY" ReadOnly="true" />
                                        <asp:BoundField DataField="Star" HeaderText="STAR" ReadOnly="true" />
                                        <asp:BoundField DataField="MarkupType" HeaderText="MARKUP TYPE" ReadOnly="true" />
                                        <asp:TemplateField HeaderText="MARKUP">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAmt" runat="server" Text='<%# Bind("MarkupAmount")%>' onKeyPress="return checkit(event)" Width="83px" MaxLength="4"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="txtAmt" ErrorMessage="Markup Requried"
                                                    Display="dynamic" ValidationGroup="group1"></asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmt" runat="server" Text='<%#Eval("MarkupAmount")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EDIT">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit" Text="EDIT"
                                                    ImageUrl="~/Images/edit.png" ToolTip="Edit" Height="16px" Width="20px"></asp:ImageButton>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="lbkUpdate" runat="server" CausesValidation="True" CommandName="Update" Text="UPDATE" ValidationGroup="group1"
                                                    ImageUrl="~/Images/ok.png" ToolTip="Update" Height="16px" Width="20px"></asp:ImageButton>
                                                <asp:ImageButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="CANCEL" ToolTip="Cancel" Height="16px" Width="20px"
                                                    ForeColor="#20313f" Font-Strikeout="False" Font-Overline="False" Font-Bold="true" ImageUrl="../../Images/cancel1.png"></asp:ImageButton>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCounter" runat="server" Text='<%#Eval("MarkupID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DELETE">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="lbkDelete" runat="server" CausesValidation="True" CommandName="Delete" Text="DELETE" ToolTip="Delete" Height="16px" Width="16px"
                                                    ImageUrl="../../Images/delete.png" OnClientClick="return confirm('Are you sure to delete it?');"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                      </div>
                     </div>
            </div> </div>
        </div>
   
    <script type="text/javascript">
        var HtlUrlBase = location.protocol + '//' + location.host;
    </script>
</asp:Content>
