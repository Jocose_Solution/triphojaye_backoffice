﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class SprReports_BookingTime
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Private ObjIntDetails As New IntlDetails()
    Dim objTktCopy As New clsTicketCopy
    Dim SelectDetails As New IntlDetails



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then
                Dim OrderId As String = Request.QueryString("OrderId")
             
                ''  BindFltHeader()
                BindLedgDetails()
                BookingTime()
                'BindMealbagg()
                ''Getting Pax Details
                'BindTravellerInformation()


                ''Getting Flight Details
                'BindFlightDetails()

                'Getting Fare Details
                lblFareInformation.Text = objTktCopy.FareDetail_N(OrderId, "")

            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub
    Public Sub BindLedgDetails()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("ledgerInfo", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            Grd_LedgerDetails.DataSource = dt2
            Grd_LedgerDetails.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub BookingTime()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("Sp_BookingTime", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@OrderID", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            GDBookingtime.DataSource = dt2
            GDBookingtime.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class

